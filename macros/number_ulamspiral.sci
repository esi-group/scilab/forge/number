// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function m = number_ulamspiral ( varargin )
    // Returns the Ulam spiral.
    //
    // Calling Sequence
    // m = number_ulamspiral ( n )
    // m = number_ulamspiral ( n , keepall )
    //
    // Parameters
    // n : a 1-by-1 matrix of floating point integers, must be odd and greater than 1
    // keepall : a 1-by-1 matrix of booleans, set to %t to keep all integers, set to %f to keep only primes (default keep=%f)
    // m : a n-by-n matrix of floating point integers
    //
    // Description
    // Returns the Ulam spiral m. 
	// Any entry of this matrix is either a zero or a prime number. 
	//
	// The process to produce this matrix is to write the integers 1, 2, 3, ... 
	// starting from the center, and circling in the counter-clockwise 
	// order. 
	// Then, only primes are kept: non-prime integers are replaced by zeros.
	//
	// According to Wikipedia, the surprising fact is that the 
	// primes "tend to line up along diagonal lines".
    //
    // Examples
    // number_ulamspiral ( 7 )
    // number_ulamspiral ( 7 , %t )
    // number_ulamspiral ( 9 )
    // number_ulamspiral ( 9 , %t )
    //
    // // Plot the Ulam spiral
    // m=number_ulamspiral(301);
    // [i,j]=find(m>0);
    // plot(i,j,"b.")
    // h = gce();
    // h.children.mark_size=1;
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Ulam_spiral

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_ulamspiral" , rhs , 1:2 )
    apifun_checklhs ( "number_ulamspiral" , lhs , 0:1 )
    //
    // Get arguments
    n = varargin ( 1 )
    keep = apifun_argindefault (  varargin , 2 , %f )
    //
    // Check type
    apifun_checktype ( "number_ulamspiral" , n , "n" , 1 , "constant" )
    apifun_checktype ( "number_ulamspiral" , keep , "keep" , 2 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( "number_ulamspiral" , n , "n" , 1 )
    apifun_checkscalar ( "number_ulamspiral" , keep , "keep" , 2 )
    //
    // Check content
    apifun_checkflint ( "number_ulamspiral" , n , "n" , 1 )
    apifun_checkrange ( "number_ulamspiral" , n , "n" , 1 , 1 , 2^53 )
    //
    if ( modulo(n,2)==0 ) then
        localstr = gettext("%s: Input argument #1 must be odd, but n=%d")
        errmsg = msprintf(localstr,"number_ulamspiral" , n)
        error ( errmsg )
    end
    //
    // Fast return for particular cases
    if ( n==1 ) then
      m = 0
      return
    end
    if ( n==3 ) then
      //
      // This allows to avoid a particular treatment 
      // for an empty k1.
      m = [5,0,3;0,0,2;7,0,0]
      return
    end
    //
    // Proceed...
    if ( ~keep ) then
        //
        // Compute the list of primes up to n^2
        allprimes = number_primes(n^2)
    end
    m = zeros(n,n)
    // The center of the matrix
    c = round(n/2)
    // step 1: towards right
    // step 2: towards up
    // step 3: towards left
    // step 4: towards down
    //mprintf("step=%d, v=%s\n",4,"1")
    m(c,c) = 1
    if ( ~keep ) then
        //
        // Set non-primes to zero
        m(c,c) = 0
    end
    // 
    // Initialize
    len = 1
    is = c
    ie = c
    js = c+1
    je = js+len-1
    ks = 2
    ke = ks+len-1
    v = (ks:ke)
    step = 1
    for i = 1 : 2*n-1
        if ( ~keep ) then
            //
            // Set non-primes to zero
            // Reduce the set of primes to improve speed.
            k1 = find(min(v)<=allprimes ,1)
            k2 = find(max(v)>=allprimes )
            k2=k2($)
            smallprimes = allprimes(k1:k2)
            if ( smallprimes == [] ) then
                v = zeros(v)
            else
                tf = specfun_ismember ( v , smallprimes )
                v(~tf)=0
            end
        end
        //mprintf("step=%d, v=%s, len=%d, \t[%d,%d]x[%d,%d]\n",step,sci2exp(v),len,is,ie,js,je)
        m(is:ie,js:je) = v
        if ( step==1 ) then
            step = 2
            ks = ke+1
            ke = ks+len-1
            is = ie-len
            ie = is+len-1
            js = je
        elseif ( step==2 ) then
            step = 3
            len = len + 1
            ks = ke+1
            ke = ks+len-1
            ie = is
            js = je-len
            je = js+len-1
        elseif ( step==3 ) then
            step = 4
            ks = ke+1
            ke = ks+len-1
            is = ie+1
            ie = is+len-1
            je = js
        else
            step = 1
            len = len + 1
            ks = ke+1
            ke = ks+len-1
            is = ie
            js = je+1
            je = js+len-1
        end
        ke=min([ke,n^2])
        je=min([je,n])
        is=max([is,1])
        if ( step == 1 ) then
            v = (ks:ke)
        elseif ( step == 2 ) then
            v = (ke:-1:ks)'
        elseif ( step == 3 ) then
            v = (ke:-1:ks)
        else
            v = (ks:ke)'
        end
    end
    //
endfunction


