// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = number_inversemod ( a , n )
  // Computes the modular multiplicative inverse.
  //
  // Calling Sequence
  // x = number_inversemod ( a , n )
  //
  // Parameters
  // a : a 1x1 matrix of floating point integers, must be positive
  // n : a 1x1 matrix of floating point integers, must be positive
  // x : a 1x1 matrix of floating point integers
  //
  // Description
  //   Returns the multiplicative inverse of a modulo n,
  //   i.e. returns x such that .
  //
  //<latex>
  //\begin{eqnarray}
  //ax \equiv 1 \pmod{n}
  //\end{eqnarray}
  //</latex>
  //
  // If no inverse exists, returns [].
  //
  //   Uses number_solvelinmod.
  //
  // Examples
  // number_inversemod ( 3 , 11 ) // 4
  //
  // // There is no inverse of 2 modulo 10
  // number_inversemod(2,10)
  //
  // Bibliography
  //   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_inversemod" , rhs , 2 )
  apifun_checklhs ( "number_inversemod" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_inversemod" , a , "a" , 1 , "constant" )
  apifun_checktype ( "number_inversemod" , n , "n" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_inversemod" , a , "a" , 1 )
  apifun_checkscalar ( "number_inversemod" , n , "n" , 2 )
  //
  // Check content
  apifun_checkflint ( "number_inversemod" , a , "a" , 1 )
  apifun_checkrange ( "number_inversemod" , a , "a" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_inversemod" , n , "n" , 2 )
  apifun_checkrange ( "number_inversemod" , n , "n" , 2 , 0 , 2^53 )
  //
  // Proceed...
  x = number_solvelinmod ( a , 1 , n )
endfunction

