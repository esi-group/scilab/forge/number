// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) INRIA - Farid BELAHCENE
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function result = number_lcm ( varargin )
    // Least common multiple.
    //
    // Calling Sequence
    //   result = number_lcm ( m , n )
    //   result = number_lcm ( m , n , method )
    //
    // Parameters
    // m : a 1x1 matrix of floating point integers
    // n : a 1x1 matrix of floating point integers
    // method : a 1x1 matrix of strings, the algorithm to use (default method="euclidian")
    // result : a 1x1 matrix of floating point integers, the least common multiple
    //
    // Description
    // Returns the least common multiple of m and n.
    // Uses number_gcd.
    // The values of the method input argument are the same as the values in number_gcd.
    //
    // If either m or n is zero, then result is zero.
    //
    // Examples
    // // Test without options
    // computed = number_lcm ( 4 , 6 ) // 12
    // computed = number_lcm ( 6 , 4 ) // 12
    // // Test with euclidian
    // computed = number_lcm ( 4 , 6 , "euclidian" ) // 12
    // computed = number_lcm ( 6 , 4 , "euclidian" ) // 12
    //
    // // With negative integers
    // computed = number_lcm ( 4 , -6 ) // 12
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
    // Copyright (C) INRIA - Farid BELAHCENE
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_lcm" , rhs , 2:3 )
    apifun_checklhs ( "number_lcm" , lhs , 0:1 )
    //
    // Get arguments
    m = varargin ( 1 )
    n = varargin ( 2 )
    method = apifun_argindefault (  varargin , 3 , "euclidian" )
    //
    // Check type
    apifun_checktype ( "number_lcm" , m , "m" , 1 , "constant" )
    apifun_checktype ( "number_lcm" , n , "n" , 2 , "constant" )
    apifun_checktype ( "number_lcm" , method , "method" , 3 , "string" )
    //
    // Check size
    apifun_checkscalar ( "number_lcm" , m , "m" , 1 )
    apifun_checkscalar ( "number_lcm" , n , "n" , 2 )
    apifun_checkscalar ( "number_lcm" , method , "method" , 3 )
    //
    // Check content
    apifun_checkflint ( "number_lcm" , m , "m" , 1 )
    apifun_checkrange ( "number_lcm" , m , "m" , 1 , -2^53 , 2^53 )
    apifun_checkflint ( "number_lcm" , n , "n" , 2 )
    apifun_checkrange ( "number_lcm" , n , "n" , 2 , -2^53 , 2^53 )
    apifun_checkoption ( "number_lcm" , method , "method" , 3 , ["euclidian" "brute" "binary" "euclidianiterative" "binaryiterative"] )
    //
    // Proceed...
    // lcm ( a , b ) = lcm ( -a , b ) = lcm ( a , -b)
    // therefore, take into account only for positive values.
    if ( m < 0 ) then
        m = -m
    end
    if ( n < 0 ) then
        n = -n
    end
    if (n==0|m==0) then
        result = 0
        return
    end
    d = number_gcd ( m , n , method )
    // Avoid unnecessary overflows
    result = n * ( m / d )
    number_checkrange ( result )
endfunction

