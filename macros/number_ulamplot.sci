// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function number_ulamplot(n)
    // Plots the Ulam spiral.
    //
    // Calling Sequence
    // number_ulamplot ( n )
    //
    // Parameters
    // n : a 1-by-1 matrix of floating point integers, must be odd and greater than 1
    //
    // Description
    // Plots the Ulam spiral.
    //
    // Examples
	// scf();
    // number_ulamplot ( 7 )
	// scf();
    // number_ulamplot ( 9 )
	// scf();
	// number_ulamplot ( 109 )
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Ulam_spiral

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_ulamplot" , rhs , 1 )
    apifun_checklhs ( "number_ulamplot" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "number_ulamplot" , n , "n" , 1 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_ulamplot" , n , "n" , 1 )
    //
    // Check content
    apifun_checkflint ( "number_ulamplot" , n , "n" , 1 )
    apifun_checkrange ( "number_ulamplot" , n , "n" , 1 , 1 , 2^53 )
    //

	A = number_ulamspiral ( n )
	k = find(A<>0)
	dims = size(A)
	[i,j] =ind2sub(dims,k)
	ii = n-i+1;
	indices = [j(:),ii(:)]
	h = gcf();
	h.children.data_bounds = [
	0 0
	n+1 n+1
	];
	drawlater()
	plot(indices(:,1),indices(:,2),"bo")
	xtitle("Ulam''spiral")
	if ( n>20 ) then
	h.children.children.children.mark_size = 1
	end
	drawnow()
endfunction
