// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function isprime = number_pseudoprime ( varargin )
  // Check if a number is a base-a Fermat pseudo prime.
  //
  // Calling Sequence
  //   isprime = number_pseudoprime ( n )
  //   isprime = number_pseudoprime ( n , a )
  //
  // Parameters
  //   n : a 1x1 matrix of floating point integers, the number to test for primality, must be positive
  //   a : a 1x1 matrix of floating point integers, the base (default 2), must be positive
  //   isprime : a 1x1 matrix of booleans
  //
  // Description
  //   Returns %f if the number is composite.
  //
  //   Returns %t if the number is a base-a pseudo-prime.
  //
  //   If n is a pseudo-prime, then n may be either prime or pseudo-prime.
  //
  //   The integer n is a base-a pseudo prime if
  //
  //   a^(n-1) = 1 (mod n).
  //
  // Examples
  // // Check for actual primes
  // number_pseudoprime ( 7 ) // %t
  // number_pseudoprime ( 5 ) // %t
  // number_pseudoprime ( 5 , 3 ) // %t
  // // Check for composite numbers
  // number_pseudoprime ( 10 ) // %f
  // number_pseudoprime ( 20 ) // %f
  // // Check for pseudo-primes : this is a known limitation of the test
  // // 341 = 11 * 31
  // number_pseudoprime ( 341 ) // %t
  // number_pseudoprime ( 561 ) // %t
  // number_pseudoprime ( 645 ) // %t
  // number_pseudoprime ( 1105 ) // %t
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_pseudoprime" , rhs , 1:2 )
  apifun_checklhs ( "number_pseudoprime" , lhs , 0:1 )
  //
  // Get arguments
  n = varargin ( 1 )
  a = apifun_argindefault (  varargin , 2 , 2 )
  //
  // Check type
  apifun_checktype ( "number_pseudoprime" , n , "n" , 1 , "constant" )
  apifun_checktype ( "number_pseudoprime" , a , "a" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_pseudoprime" , n , "n" , 1 )
  apifun_checkscalar ( "number_pseudoprime" , a , "a" , 2 )
  //
  // Check content
  apifun_checkflint ( "number_pseudoprime" , n , "n" , 1 )
  apifun_checkrange ( "number_pseudoprime" , n , "n" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_pseudoprime" , a , "a" , 2 )
  apifun_checkrange ( "number_pseudoprime" , a , "a" , 2 , 0 , 2^53 )
  //
  // Proceed...
  b = number_powermod ( a , n - 1 , n )
  if ( b <> 1 ) then
    isprime = %f
    // Definitely
  else
    isprime = %t
    // We hope !
  end
endfunction

