// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function digits = number_tobary ( varargin )
  //  Decompose a number into arbitrary basis.
  //
  // Calling Sequence
  //   digits = number_tobary ( n )
  //   digits = number_tobary ( n , basis )
  //   digits = number_tobary ( n , basis , order )
  //   digits = number_tobary ( n , basis , order , p )
  //
  // Parameters
  //   n : a 1x1 matrix of floating point integers, the integer to decompose, must be positive
  //   basis : a 1x1 matrix of floating point integers, the basis, must be positive, (default basis=2)
  //   order : a 1x1 matrix of strings, the order (default order="littleendian")
  //   p : a 1x1 matrix of floating point integers, the number of digits on output (default p=0)
  //   digits : a p-by-1 matrix of floating point integers, the digits.
  //
  // Description
  //   Returns a column matrix of digits of the decomposition of
  //   n in base b, i.e. decompose n as
  //   <programlisting>
  //   n = d(1) b^(p-1) + d(2) b^(p-2) + ... + d(p) b^0.
  //   </programlisting>
  //   This order is little endian order since the last digit
  //   is associated with b^0.
  //   If "bigendian" order is chosen, returns the digits d so that
  //   <programlisting>
  //   n = d(1) b^0 + d(2) b^1 + ... + d(p) b^(p-1),
  //   </programlisting>
  //   i.e. the last digit is associated with b^(p-1).
  //
  //   If p=0, returns the least possible number of digits.
  //   If p is a positive integer, returns p digits.
  //   If the integer cannot be stored in p digits, generates an error.
  //   If the integer is larger than p digits, pad with zeros.
  //
  // Examples
  //   number_tobary (4,2) // [1 0 0]
  //   number_tobary (4,2,"bigendian") // [0 0 1]
  //   number_tobary (4,2,"littleendian",8) // [0,0,0,0,0,1,0,0]
  //
  // Bibliography
  //   Monte-Carlo methods in Financial Engineering, Paul Glasserman
  //
  // Authors
  // Copyright (C) 2008-2009 - INRIA - Michael Baudin
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_tobary" , rhs , 1:4 )
  apifun_checklhs ( "number_tobary" , lhs , 1 )
  //
  n = varargin ( 1 )
  basis = apifun_argindefault (  varargin , 2 , 2 )
  order = apifun_argindefault (  varargin , 3 , "littleendian" )
  p = apifun_argindefault (  varargin , 4 , 0 )
  //
  apifun_checktype ( "number_tobary" , n ,     "n" ,     1 , "constant" )
  apifun_checktype ( "number_tobary" , basis , "basis" , 2 , "constant" )
  apifun_checktype ( "number_tobary" , order , "order" , 3 , "string" )
  apifun_checktype ( "number_tobary" , p ,     "p" ,     4 , "constant" )
  //
  if ( n <> [] ) then
    apifun_checkscalar ( "number_tobary" , n ,     "n" ,     1 )
  end
  apifun_checkscalar ( "number_tobary" , basis , "basis" , 2 )
  apifun_checkscalar ( "number_tobary" , order , "order" , 3 )
  apifun_checkscalar ( "number_tobary" , p ,     "p" ,     4 )
  //
  if ( n <> [] ) then
    apifun_checkgreq ( "number_tobary" , n , "n" , 1 , 0 )
    apifun_checkflint ( "number_tobary" , n , "n" , 1 )
  end
  apifun_checkgreq ( "number_tobary" , basis , "basis" , 2 , 2 )
  apifun_checkflint ( "number_tobary" , basis , "basis" , 2 )
  apifun_checkoption ( "number_tobary" , order , "order" , 3 , ["littleendian" "bigendian"] )
  apifun_checkgreq ( "number_tobary" , p , "p" , 4 , 0 )
  apifun_checkflint ( "number_tobary" , p , "p" , 4 )
  //
  // Proceed...
  if ( n == [] ) then
    digits = []
    return
  end
  //
  // Compute in littleendian
  if (n==0) then
    digits = 0;
  else
    jmax = int(log(n)/log(basis));
    q = int(basis^jmax);
    for j=1:jmax+1
      aj = int(n/q);
      digits(j) = aj;
      n = n - q * aj;
      q = q/basis;
    end
  end
  // Reverse order if necessary
  if ( order == "bigendian" ) then
    q = size(digits,"*")
    digits(q:-1:1) = digits(1:q)
  end
  // Pad up to p digits
  if ( p > 0 ) then
    q = size(digits,"*")
    if ( q > p ) then
      errmsg = msprintf(gettext("%s: Unexpected value %d to convert: %d bits are required, which is larger than maximum %d."), "number_tbgui_convertobin", n , q , p);
      error(errmsg)
    end
    select order
    case "littleendian"
      // Put zeros at beginning
      digits = [
      zeros(p-q,1)
      digits
      ];
    case "bigendian"
      // Put zeros at end
      digits = [
      digits
      zeros(p-q,1)
      ];
    else
      errmsg = msprintf(gettext("%s: Unexpected order: %d."), "number_tbgui_littlebig", number_baryguih.hlittlebigendian.Value);
      error(errmsg)
    end
  end
endfunction

