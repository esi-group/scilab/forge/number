// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = number_solvelinmod(a, b, n)
  // Solves a linear modular equation.
  //
  // Calling Sequence
  //   x = number_solvelinmod ( a , b , n )
  //
  // Parameters
  //   a : a 1x1 matrix of floating point integers, must be positive
  //   b : a 1x1 matrix of floating point integers, must be positive
  //   n : a 1x1 matrix of floating point integers, must be positive
  //   x : a 1x1 matrix of floating point integers
  //
  // Description
  // Returns the row vector of all x such that
  //
  //<latex>
  //\begin{eqnarray}
  //ax \equiv b \pmod{n}
  //\end{eqnarray}
  //</latex>
  //
  // Examples
  // number_solvelinmod ( 14 , 30 , 100 ) // [95 45]
  // number_solvelinmod ( 3 , 4 , 5 ) // 3
  // number_solvelinmod ( 3 , 5 , 6 ) // []
  // number_solvelinmod ( 3 , 6 , 9 ) // [2 5 8]
  //
  // Bibliography
  //   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_solvelinmod" , rhs , 3 )
  apifun_checklhs ( "number_solvelinmod" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_solvelinmod" , a , "a" , 1 , "constant" )
  apifun_checktype ( "number_solvelinmod" , b , "b" , 2 , "constant" )
  apifun_checktype ( "number_solvelinmod" , n , "n" , 3 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_solvelinmod" , a , "a" , 1 )
  apifun_checkscalar ( "number_solvelinmod" , b , "b" , 2 )
  apifun_checkscalar ( "number_solvelinmod" , n , "n" , 3 )
  //
  // Check content
  apifun_checkflint ( "number_solvelinmod" , a , "a" , 1 )
  apifun_checkrange ( "number_solvelinmod" , a , "a" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_solvelinmod" , b , "b" , 2 )
  apifun_checkrange ( "number_solvelinmod" , b , "b" , 2 , 0 , 2^53 )
  apifun_checkflint ( "number_solvelinmod" , n , "n" , 3 )
  apifun_checkrange ( "number_solvelinmod" , n , "n" , 3 , 0 , 2^53 )
  //
  // Proceed...
  x = [];
  [d, xp, yp] = number_extendedeuclid(a, n);

  isdivisor = number_isdivisor(d, b);

  if isdivisor then

    x0 = pmodulo(xp * b / d, n);

    for i = 0 : d - 1

      x (1, $+1) = pmodulo(x0 + i*n/d, n);

    end

  end
endfunction

