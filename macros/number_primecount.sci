// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = number_primecount ( x )
  // Returns the value of the prime-counting function.
  //
  // Parameters
  // x : a matrix of floating point integers, the points where to evaluate the function, must be positive
  // y : a matrix of floating point integers, the function value
  //
  // Description
  //   The prime-counting function of x is equal to the number of prime numbers
  //   less than or equal to x.
  //
  // Examples
  //   x = 1:60;
  //   y = number_primecount ( x );
  //   plot(x,y,"bo")
  //
  // Bibliography
  // http://en.wikipedia.org/wiki/Prime-counting_function
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
  apifun_checkrhs ( "number_primecount" , rhs , 1 )
  apifun_checklhs ( "number_primecount" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_primecount" , x , "x" , 1 , "constant" )
  //
  // Check size
  // Nothing to do
  //
  // Check content
  apifun_checkflint ( "number_primecount" , x , "x" , 1 )
  apifun_checkrange ( "number_primecount" , x , "x" , 1 , 0 , 2^53 )
  //
  // Proceed...
  n = max(x)
  p = number_primes(n)
  // TODO : find a vectorized way
  for k = 1 : size(x,"*")
    y(k) = size(find(p<=x(k)),"*")
  end
endfunction

