// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) INRIA - Farid BELAHCENE
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = number_factor ( varargin )
    // Factors a number into primes.
    //
    // Calling Sequence
    //   fac = number_factor ( n )
    //   [ fac , status ] = number_factor ( n )
    //   [ fac , status ] = number_factor ( n , method )
    //   [ fac , status ] = number_factor ( n , method , imax )
    //   [ fac , status ] = number_factor ( n , method , imax , verbose )
    //   [ fac , status ] = number_factor ( n , method , imax , verbose , pollardseeds )
    //
    // Parameters
    //  n : a 1x1 matrix of floating point integers
    //  method : a 1x1 matrix of strings, the method to use (default "fasttrialdivision"). Available are method = "fasttrialdivision", "trialdivision", "memorylesstd", "fermat", "pollard-rho".
    //  imax : a 1-by-1 matrix of floating point integers, the number of steps in the loop (default imax=10000)
    //  verbose : a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)
    //  pollardseeds : a m-by-1 matrix of floating point integers, the seeds for Pollard's rho. Only for method="pollard-rho". Default pollardseeds = [-3 1 3 5].
    //  fac : a 1-by-m matrix of floating point integers, the prime numbers which decompose n.
    //  status : %t if the factorization is a success, %f if the factorization is a failure. If method="pollard-rho", then status=%f.
    //
    // Description
    //   Returns the prime factors of n, as a row matrix.
    //   The output argument fac is so that each entry is a prime number and
    //
    //<screen>
    //prod(fac)==n
    //</screen>
    //
    //   The primes factors are not necessarily unique.
    //
    //   If n=0, 1, 2 or 3, then returns fac=n and status = %t.
    //
    //  In case of success, all factors are returned and success = %t.
    //
    //  In case of failure, if there is one output argument, an error
    //  is generated.
    //
    //  In case of failure, if there are two output arguments,
    //  success = %f and the factors which were found are returned in fac.
    //  This feature allows to retrieve the factors which could be found (partial success), even
    //  if, globally, the algorithm failed.
    //
    //  If method="pollard-rho", then status=%f or a warning is produced if the status output
    //  argument is not provided.
    //  This is because Pollard-rho is not 100% guaranteed to produce a set of prime factors.
    //  Although this method is often able to produce factors of large composite numbers,
    //  the algorithm is not able, by itself, to identify prime factors.
    //  This is why the status indicates that, in general, the algorithm fails.
    //
    //  Any input argument equal to the empty matrix is replaced by its default value.
    //
    // The list of available methods is the following.
    //  <variablelist>
    //    <varlistentry>
    //      <term> method="trialdivision" </term>
    //      <listitem>
    //      <para>
    //      Uses the Trial Division method, which is known as the
    //      simplest, and the least efficient method.
    //      This is Algorithm A, "Factoring by division", section 4.5.4 in Knuth's TAO.
    //      </para>
    //      </listitem>
    //    </varlistentry>
    //    <varlistentry>
    //      <term> method="fasttrialdivision" </term>
    //      <listitem>
    //      <para>
    //      Uses a vectorized trial division algorithm.
    //      Uses an array with size <literal>sqrt(n)</literal>,
    //      which implies that
    //      it may fail for large integers.
    //      This implementation is due to F. Belahcene.
    //      </para>
    //      </listitem>
    //    </varlistentry>
    //    <varlistentry>
    //      <term> method="memorylesstd" </term>
    //      <listitem>
    //      <para>
    //      Uses the Trial Division method, without any list of primes.
    //      This is Algorithm A, "Factoring by division", section 4.5.4 in Knuth's TAO.
    //      We divide by prime until the remainder is not zero.
    //      The following primes are tested in order:
    //      2 3 5 7 11 13 17 ...
    //      After the prime p=5 has been used, we add 2, then 4, then 2, ...
    //      </para>
    //      </listitem>
    //    </varlistentry>
    //    <varlistentry>
    //      <term> method="fermat" </term>
    //      <listitem>
    //      <para>
    //      Uses the Fermat's method, without sieve.
    //      </para>
    //      </listitem>
    //      </varlistentry>
    //      <varlistentry>
    //      <term> method="pollard-rho" </term>
    //      <listitem>
    //      <para>
    //      Uses the Pollard-rho method with the collection of offsets
    //      associated with the "-pollardoffsets" option.
    //      The <literal>c</literal> integers in the "-pollardoffsets"
    //      are all used ( in order ) in the pseudo-random number generator
    //      <literal>x^2-c</literal>.
    //      This algorithm uses the grand function to generate random integers (option "uin").
    //      </para>
    //      </listitem>
    //    </varlistentry>
    //  </variablelist>
    //
    //  With Pollard's rho method, failure is obtained either when the
    //  number is prime or composite (i.e. failure is not a mark
    //  for primality or compositeness, it is just a failure).
    //  In that case, running the algorithm again, one or more time,
    //  may produce additionnal factors.
    //
    // Examples
    // number_factor ( 2^2-1 ) // 3
    // number_factor ( 2^3-1 ) // 7
    // number_factor ( 2^4-1 ) // [3 5]
    // number_factor ( 2^5-1 ) // 31
    //
    // // With a negative integer n to factor, the first factor is negative.
    // number_factor ( -12 ) // [-2 2 3]
    //
    // // A difficult number of fasttrialdivision
    // stacksize("max");
    // number_factor ( 2^42-1 , "fasttrialdivision" )
    // // Two loops are not sufficient: an error is generated
    // number_factor ( 2^42-1 , "fasttrialdivision" , 2 )
    // // Two loops are not sufficient: the status is false (and not error is generated)
    // [fac,status]=number_factor ( 2^42-1 , "fasttrialdivision" , 2 )
    // // See the details of the steps
    // number_factor ( 2^42-1 , "fasttrialdivision" , [] , %t )
    //
    // // Use Pollard's rho.
    // // See that Pollard-rho produces the composite factor 49
    // // and produces a warning (this is the expected behavior).
    // grand("setsd",0);
    // fac = number_factor(7^5,"pollard-rho")
    //
    // // Use various seeds for Pollard's rho.
    // // 2^20-1 = 3*5*5*11*31*41
    // // See that Pollard-rho produces the composite factor 15 :
    // grand("setsd",0);
    // [fac,status] = number_factor(2^20-1,"pollard-rho",[],[],[1 3 5])
    // // See that Pollard-rho produces the composite factor 25 :
    // grand("setsd",0);
    // [fac,status] = number_factor(2^20-1,"pollard-rho",[],[],-1);
    //
    // // Factor Mersenne numbers
    // algos = [
    //  "fasttrialdivision"
    //  "trialdivision"
    //  "memorylesstd"
    //  "fermat"
    //  "pollard-rho"
    // ];
    // for n = [2^7-1 2^8-1 2^9-1 2^11-1 2^12-1 2^13-1 2^14-1 2^15-1]
    //   mprintf("n=%d :\n",n);
    //   for method = algos'
    //     f = number_factor(n,method);
    //     mprintf("  %20s : %s\n",method,sci2exp(f));
    //   end
    // end
    //
    // Bibliography
    // "The Art of Computer Programming", Volume 2, Seminumerical Algorithms, Donald Knuth, Addison-Wesley
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
    // Copyright (C) INRIA - Farid BELAHCENE
    //

    [lhs,rhs]=argn();
    apifun_checkrhs ( "number_factor" , rhs , 1:5 )
    apifun_checklhs ( "number_factor" , lhs , 1:2 )
    //
    // Get arguments
    n = varargin ( 1 )
    method = apifun_argindefault (  varargin , 2 , "fasttrialdivision" )
    imax = apifun_argindefault (  varargin , 3 , 10000 )
    verbose = apifun_argindefault (  varargin , 4 , %f )
    pollardseeds = apifun_argindefault (  varargin , 5 , [-3 1 3 5] )
    //
    // Check arguments
    //
    // Check type
    apifun_checktype ( "number_factor" , n , "n" , 1 , "constant" )
    apifun_checktype ( "number_factor" , method , "method" , 2 , "string" )
    apifun_checktype ( "number_factor" , imax , "imax" , 3 , "constant" )
    apifun_checktype ( "number_factor" , verbose , "verbose" , 4 , "boolean" )
    apifun_checktype ( "number_factor" , pollardseeds , "pollardseeds" , 5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_factor" , n , "n" , 1 )
    apifun_checkscalar ( "number_factor" , method , "method" , 2 )
    apifun_checkscalar ( "number_factor" , imax , "imax" , 3 )
    apifun_checkscalar ( "number_factor" , verbose , "verbose" , 4 )
    apifun_checkvector ( "number_factor" , pollardseeds , "pollardseeds" , 5 , size(pollardseeds,"*") )
    //
    // Check content
    apifun_checkflint ( "number_factor" , n , "n" , 1 )
    apifun_checkrange ( "number_factor" , n , "n" , 1 , -2^53 , 2^53 )
    apifun_checkoption ( "number_factor" , method , "method" , 2 , ["fasttrialdivision","trialdivision","memorylesstd","fermat","pollard-rho"] )
    apifun_checkrange ( "number_factor" , imax , "imax" , 3 , 0 , 2^53 )
    apifun_checkflint ( "number_factor" , pollardseeds , "pollardseeds" , 5 )
    apifun_checkrange ( "number_factor" , pollardseeds , "pollardseeds" , 5 , -2^53 , 2^53 )
    //
    // Proceed...
    // The factors are the same, except the sign of the first factor
    if ( n < 0 ) then
        n = -n
        chsi = %t
    else
        chsi = %f
    end
    //
    // Quick return if possible
    if ( or(n==[0 1 2 3]) ) then
        select lhs
        case 1 then
            varargout ( 1 ) = n
        case 2 then
            varargout ( 1 ) = n
            varargout ( 2 ) = %t
        end
        return
    end
    select method
    case "trialdivision"
        [ fac , status ] = factor_trialdivision ( n , imax , verbose )
    case "fasttrialdivision"
        [ fac , status ] = factor_fasttd ( n , imax , verbose )
    case "memorylesstd"
        [ fac , status ] = factor_memorylesstd ( n , imax , verbose )
    case "fermat"
        [ fac , status ] = factor_fermat ( n , imax , verbose )
    case "pollard-rho"
        [ fac , status ] = factor_pollard ( n , imax , pollardseeds , verbose )
    else
        errmsg = sprintf ( gettext ( "%s: Unknown method %s" ) , "number_factor" , method )
        error(errmsg)
    end
    if ( ~status & ( lhs == 1 ) ) then
        if (method=="pollard-rho") then
            localstr = gettext("%s: The algorithm may have failed to produce prime factors.")
            warning(msprintf(localstr, "number_factor" ));
        else
            localstr = gettext("%s: The algorithm failed. Try to increase the value of the imax option.")
            error(msprintf(localstr, "number_factor" ));
        end
    end
    //
    // Update sign of the first factor if necessary
    if ( chsi ) then
        fac(1) = -fac(1)
    end
    //
    // Fill output arguments
    select lhs
    case 1 then
        varargout ( 1 ) = fac
    case 2 then
        varargout ( 1 ) = fac
        varargout ( 2 ) = status
    end
endfunction
//
// factor_fasttd --
//   Uses a vectorized trial division algorithm.
//   Uses an array with size sqrt(n).
// Author : F.Belahcene
//
function [ fac , status ] = factor_fasttd ( x , imax , verbose )
    s = floor ( sqrt ( n ) )
    number_factorlog ( verbose , msprintf ( "Computing primes less than %s\n" , string ( s ) ) )
    xprimefact = number_primes ( s )
    fac = []
    status = %f
    for i = 1 : imax
        number_factorlog ( verbose , msprintf ( "Loop #%d/%d, x=%s\n" , i , imax , string ( x ) ) )
        if ( x==1 ) then
            number_factorlog ( verbose , msprintf ( "Found x=1, done.\n" ) )
            status = %t
            break
        end
        xdiv = x ./ xprimefact
        xprimefact_ind = find(xdiv-floor(xdiv)==0)
        if ( xprimefact_ind <> [] )
            xprimefact = xprimefact ( xprimefact_ind )
            number_factorlog ( verbose , msprintf ( "Found factors %s \n" , strcat ( string ( xprimefact ) , " " ) ) )
            fac = [xprimefact fac]
            x = x/prod(xprimefact)
        else
            number_factorlog ( verbose , msprintf ( "Found no divisor, done.\n" ) )
            fac=[x fac]
            status = %t
            break
        end
    end
    if ( ~status ) then
        number_factorlog ( verbose , msprintf ( "Failed to factor completely, x = %s.\n" , string ( x ) ) )
        fac ( 1 , $ + 1 ) = x
    end
    fac = gsort(fac,'c','i')
endfunction

//
// factorization_trialdivision --
//   Returns the list of prime factors of n
//   Uses the Trial Division method, which is known as the
//   simplest, and the least efficient method.
// Arguments:
//   n : the positive integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
// Notes:
//   This is Algorithm A, "Factoring by division", section 4.5.4 in Knuth's TAO
//
function [ fac , status ] = factor_trialdivision ( n , imax , verbose )
    fac = []
    // Get all primes up to sqrt(n) + 1
    s = floor ( sqrt ( n ) ) + 1
    number_factorlog ( verbose , msprintf ( "Computing primes less than %s\n" , string ( s ) ) )
    prms = number_primes ( s )
    if ( prms( $ ) < s ) then
        // Find a prime greater or equal to s
        p = prms( $ )
        while ( %t )
            p = p + 2
            [ isprime , status ] = number_factor ( p )
            if ( ~status ) then
                status = %f
                break
            end
            if ( isprime ) then
                prms ( $ + 1 ) = p
                break
            end
        end
    end
    // Divide by prime until the remainder is not zero
    u = n
    // ip is the index of the current prime
    ip = 1
    p = prms ( ip )
    nbp = size ( prms , "*" )
    status = %f
    for i = 1 : imax
        number_factorlog ( verbose , msprintf ( "Loop #%d/%d, u=%s, trying p = %s\n" , i , imax , string ( u ) , string ( p ) ) )
        if ( u == 1 ) then
            number_factorlog ( verbose , msprintf ( "Found u=1, done.\n" ) )
            status = %t
            break
        end
        r = modulo ( u , p )
        if ( r==0 ) then
            number_factorlog ( verbose , msprintf ( "Found divisor p=%s.\n" , string ( p ) ) )
            fac ( 1 , $ + 1 ) = p
            u = u / p
        else
            // p does not divide u
            if ( ip < nbp ) then
                // There are primes left to test
                ip = ip + 1
                p = prms ( ip )
                number_factorlog ( verbose , msprintf ( "Trying new divisor #%d, p=%s.\n" , ip , string ( p ) ) )
            else
                // There are no primes left to test : the last number n is a prime
                number_factorlog ( verbose , msprintf ( "No prime left to test, done.\n" ) )
                fac ( 1 , $ + 1 ) = u
                status = %t
                break
            end
        end
    end
    if ( u == 1 ) then
        number_factorlog ( verbose , msprintf ( "Found u=1, done.\n" ) )
        status = %t
        return
    end
    if ( ~status ) then
        number_factorlog ( verbose , msprintf ( "Failed to factor completely, u = %s.\n" , string ( u ) ) )
        fac ( 1 , $ + 1 ) = u
    end
endfunction
//
// factor_fermat --
//   Returns the list of prime factors of n
//   Uses the Fermat's method.
//   The first number in the list is allways 1.
// Arguments:
//   n : the positive integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
//
function [ fac , status ] = factor_fermat ( n , imax , verbose )
    // Factors by two until n is odd
    [ twofactors , status ] = factor_bytwo ( n , imax , verbose )
    if ( ~status ) then
        fac = []
        return
    end
    fac = twofactors ( 1 : $ - 1 )
    u = twofactors ( $ )
    if ( u==1 ) then
        return
    end
    // Compute factors for odd number u
    [ otherfactors , status ] = factor_fermatdo ( u , imax )
    fac = [ fac otherfactors ]
    fac = gsort ( fac , "g" , "i" )
endfunction
//
// factor_fermatdo --
//   Performs the algorithm recursively on the
//   given odd number.
//   Returns only the factors which are different from 1.
//   This is Algorithm C, "Factoring by addition and substraction",
//   section 4.5.4 in Knuth's TAO
// Arguments:
//   n : the positive, odd, integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
// Note :
// The algorithm is very slow to factor 33554431.
// factor_fermatdo ( 33554431 )
//
function [ fac , status ] = factor_fermatdo ( n , imax , verbose )
    [ p1 , p2 , status ] = factor_fermatcore ( n , imax )
    if ( ~status ) then
        fac = n
        return
    end
    isprime = ( ( p1==1 ) & ( p2==n ) )
    // Factor p1 and p2 recursively, if required
    fac = []
    if ( isprime ) then
        // If n is prime, there is no need for recursive calls
        // since n = 1 x n (where the 1 factor is discarded)
        fac ( 1 , $ + 1 ) = p2
    else
        // Factorize p1
        [ otherfactors1 , status ] = factor_fermatdo ( p1 , imax )
        if ( ~status ) then
            fac = [p1 p2]
            return
        end
        // Be p1 prime or composite, add its factors.
        fac = [ fac otherfactors1 ]
        // Factorize p2
        [ otherfactors2 , status ] = factor_fermatdo ( p2 , imax )
        if ( ~status ) then
            fac = [p1 p2]
            return
        end
        // Be p2 prime or composite, add its factors.
        fac = [ fac otherfactors2 ]
    end
    status = %t
endfunction
//
// factor_fermatcore --
//   Given an odd integer, returns two
//   factors p1 and p2 so that n = p1 x p2.
//   The factors may be non prime.
//   This is Algorithm C, "Factoring by addition and substraction",
//   section 4.5.4 in Knuth's TAO
//   Returns p1 = 0 and p2 = 0 if the procedure fails
//   before imax iterations.
// Arguments:
//   n : the positive, odd, integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
// Note :
// * The algorithm is very slow to factor 33554431.
//   factor_fermatdo ( 33554431 )
// * factor_fermatcore is faster than factor_fermatcore2
//
function [ p1 , p2 , status ] = factor_fermatcore ( n , imax , verbose )
    a = ceil(sqrt(n))
    status = %f
    for i = 1 : imax
        number_factorlog ( verbose , msprintf ( "Loop #%d/%d, a = %d.\n" , i , imax , a ) )
        number_checkrange ( a * a )
        b2 = a * a - n
        // Computes if b is a square
        b = ceil(sqrt(b2))
        //mprintf ( "a=%d, b=%d, n=%d, a^2 - b^2=%d, |n-(a^2-b^2)|=%d\n", a, b, n, a^2 - b^2 , abs(n-a^2+b^2))
        if ( b^2 == b2 ) then
            // b2 is a square so that b2 = b^2 and
            // n = a^2 - b^2
            number_factorlog ( verbose , msprintf ( "Found b=%d, break.\n" , b ) )
            status = %t
            break
        end
        a = a + 1
    end
    // n = (a - b) * (a + b)
    if ( status ) then
        p1 = a - b
        p2 = a + b
        number_factorlog ( verbose , msprintf ( "Found divisors p1=%d, p2=%d, done.\n" , p1 , p2 ) )
    else
        number_factorlog ( verbose , msprintf ( "Found no divisor for n=%d.\n" , n ) )
        p1 = 0
        p2 = 0
    end
endfunction
//
// factor_fermatcore2 --
//   Given an odd integer, returns two
//   factors p1 and p2 so that n = p1 x p2.
//   The factors may be non prime.
//   This is Algorithm C, "Factoring by addition and substraction",
//   section 4.5.4 in Knuth's TAO
//   This is an implementation as close as possible to Knuth's
//   algorithm
//   Returns p1 = 0 and p2 = 0 if the procedure fails
//   before imax iterations.
// Arguments:
//   n : the positive, odd, integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
// Note :
// The algorithm is very slow to factor 33554431.
// factor_fermatcore2 ( 33554431 )
//
function [ p1 , p2 , status ] = factor_fermatcore2 ( n , imax , verbose )
    step = 1
    status = %f
    for i = 1 : imax
        select step
        case 1
            // Initialize
            xp = 2 * ceil(sqrt(n)) + 1
            yp = 1
            r = (ceil(sqrt(n)))^2 - n
            step = 2
        case 2
            // Test r
            if ( r <= 0 ) then
                step = 4
            else
                step = 3
            end
        case 3
            // Step y
            r = r - yp
            yp = yp + 2
            step = 2
        case 4
            // Done ?
            if ( r == 0 ) then
                status = %t
                break
            else
                step = 5
            end
        case 5
            // Step x
            r = r + xp
            xp = xp + 2
            step = 3
        end
    end
    if ( status ) then
        p1 = ( xp - yp ) / 2
        p2 = ( xp + yp - 2 ) / 2
    else
        p1 = 0
        p2 = 0
    end
endfunction
//
// factor_pollard --
//   Returns the list of prime factors of n
//   Uses the Pollard method.
//   Returns 0 if the method fails to compute the factors.
// Arguments:
//   n : the positive integer number to factor
//   imax : maximum number of loops in Floyd's loop.
//
function [ fac , status ] = factor_pollard ( n , imax , pollardseeds , verbose )
    fac = []
    // Factors by two until n is odd
    [ twofactors , status ] = factor_bytwo ( n , imax , verbose )
    if ( ~status ) then
        fac = []
        return
    end
    twolist = twofactors ( 1 : $-1 )
    fac  = [ fac twolist ]
    u = twofactors ( $ )
    // Compute factors for odd number u
    if ( u <> 1 ) then
        [ otherfactors , status ] = pollardrhoglobal ( u , imax , pollardseeds , verbose )
        fac = [ fac otherfactors ]
        fac = gsort ( fac , "g" , "i" )
    end
endfunction
//
// pollardrhoglobal --
//   Uses the Pollard - rho method to find new factors
//   until all factors have been found.
//   Returns status = %f if the method fails to compute the factors.
// Arguments:
//   n : the positive integer number to factor
//   imax : maximum number of loops in Floyd's loop.
// Bibliography
//   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
//
function [ fac , status ] = pollardrhoglobal ( n , imax , pollardseeds , verbose )
    number_factorlog ( verbose , msprintf ( "pollardrhoglobal: n=%s\n" , string(n) ) )
    u = n
    fac = []
    simax = floor ( sqrt(imax) )
    for j = 1 : simax
        number_factorlog ( verbose , msprintf ( "Global loop #%d/%d, factoring u = %s\n" , ..
        j , simax , string ( u ) ) )
        [ d , status ] = factor_pollardrho ( u , simax , pollardseeds , verbose )
        if ( status ) then
                number_factorlog ( verbose , msprintf ( "Factoring divisor %s.\n" , string ( d ) ) )
                [ dfactors , status ] = pollardrhoglobal ( d , imax , pollardseeds , verbose )
                if ( status ) then
                    number_factorlog ( verbose , msprintf ( "Factoring of divisor OK : add [%s] to the list .\n" , strcat ( string ( dresult ) , " " ) ) )
                    fac = [ fac dfactors ]
                    u = u / prod ( dfactors )
                else
                    number_factorlog ( verbose , msprintf ( "Failure of Pollard-rho on factoring %s.\n" , string(d) ) )
                    fac ( 1 , $ + 1 ) = d
                    u = u / d
                end
        else
            number_factorlog ( verbose , msprintf ( "Failure of Pollard-rho on factoring n=%s\n",string(n) ) )
            fac ( 1 , $ + 1 ) = u
            return
        end
    end
    status = %f
    fac ( 1 , $ + 1 ) = u
endfunction
//
// factor_pollardrho --
//   Returns the list of prime factors of the odd number n
//   Uses the Pollard - rho method.
//   Returns status = %t if the method was able to compute a non-trivial (i.e. not
//   equal to 1 and not equal to n) factor.
//   Returns status = %f if the method fails to compute a non-trivial factor.
// Arguments:
//   n : the positive integer number to factor
//   imax : maximum number of loops in Floyd's loop.
// Bibliography
//   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
//
function [ d , status ] = factor_pollardrho ( n , imax , pollardseeds , verbose )
    number_factorlog ( verbose , msprintf ( "factor_pollardrho: factoring n=%d\n" , n ) )
    if ( n == 1 ) then
        fac = 1
        status = %t
        return
    end
    // Try several values of c before declaring we are done
    seeds = pollardseeds
    for c = seeds
        i = 1
        xi = grand(1,1,"uin",0,n-1)
        y = xi
        k = 2
        // Record the previous states in the algorithm.
        previous = xi
        for j = 1 : imax
            i = i + 1
            xn = modulo ( xi^2 - c , n )
        number_factorlog ( verbose , msprintf ( "seed=%d, x=%d, Local loop #%d/%d, x=%d\n" , c, xi, j , imax ,xn) )
            d = number_gcd ( y - xn , n )
            if ( ( d <> 1 ) & ( d <> n ) ) then
                number_factorlog ( verbose , msprintf ( "Found new factor: d=%d\n" , d ) )
                status = %t
                return
            end
            if ( i == k ) then
                y = xn
                k = 2 * k
            end
            xi = xn
            if ( find(xn==previous)<>[] ) then
                number_factorlog ( verbose , msprintf ( "Stationnary cycle: break.\n" ) )
                break
            else
                previous($+1)=xn
            end
        end
    end
    status = %f
    d = []
endfunction
//
// factor_bytwo --
//   Divide the given n by 2 until the number becomes odd.
//   Returns the list of 2 which were found plus the odd
//   number at the end of the list.
// Arguments:
//   n : the positive integer number to factor
//
function [ fac , status ] = factor_bytwo ( n , imax , verbose )
    fac = []
    // Divide by 2 until the remainder is not zero
    u = n
    status = %f
    for i = 1 : imax
        number_factorlog ( verbose , msprintf ( "Loop #%d/%d, u=%d\n" , i , imax , u ) )
        if ( modulo ( u , 2 ) == 0 ) then
            number_factorlog ( verbose , msprintf ( "Found divisor %d\n" , 2 ) )
            fac ( 1 , $ + 1 ) = 2
            u  = u / 2
        else
            // Number is odd
            number_factorlog ( verbose , msprintf ( "No divisor left, done.\n" ) )
            status = %t
            break
        end
    end
    fac ( 1 , $ + 1 ) = u
endfunction
//
// factorization_memorylesstd --
//   Uses the Trial Division method, without any list of primes.
// Arguments:
//   n : the positive integer number to factor
//   imax : the maximum number of iterations in the search for a divisor
// Note:
//   This is Algorithm A, "Factoring by division", section 4.5.4 in Knuth's TAO
// Divide by prime until the remainder is not zero
// * add2 is a boolean which allows to compute the next prime
//   The following primes are tested in order:
//   2 3 5 7 11 13 17 ...
//   After the prime p=5 has been used, we add 2, then 4, then 2.
// * passedS is true when we have tried a prime greater or equal to sqrt(n)
//
function [ fac , status ] = factor_memorylesstd ( n , imax , verbose )
    fac = []
    // Get all primes up to sqrt(n)
    s = ceil ( sqrt ( n ) )
    u = n
    p = 2
    passedS = %f
    add2 = %t
    status = %f
    for i = 1 : imax
        number_factorlog ( verbose , msprintf ( "Loop #%d/%d, u=%s\n" , i , imax , string ( u ) ) )
        if ( u == 1 ) then
            number_factorlog ( verbose , msprintf ( "Found u=1, done.\n" ) )
            status = %t
            break
        end
        r = modulo ( u , p )
        if ( r==0 ) then
            number_factorlog ( verbose , msprintf ( "Found divisor %s\n" , string( p )) )
            fac ( 1 , $ + 1 ) = p
            u = u / p
        else
            // p does not divide u
            if ( ~passedS ) then
                // There are primes left to test
                if ( p == 2 ) then
                    p = 3
                elseif ( p == 3 ) then
                    p = 5
                elseif ( add2 ) then
                    p = p + 2
                    add2 = %f
                else
                    p = p + 4
                    add2 = %t
                end
                if ( p >= s ) then
                    passedS = %t
                end
                number_factorlog ( verbose , msprintf ( "Next divisor: %s\n" , string ( p ) ) )
            else
                // There are no primes left to test : the last number n is a prime
                number_factorlog ( verbose , msprintf ( "No prime left to test, done.\n" ) )
                fac ( 1 , $ + 1 ) = u
                status = %t
                break
            end
        end
    end
    if ( ~status ) then
        number_factorlog ( verbose , msprintf ( "Failed to factor completely, u = %s.\n" , string ( u ) ) )
        fac ( 1 , $ + 1 ) = u
    end
endfunction

function number_factorlog ( verbose , msg )
    if ( verbose ) then
        mprintf ( "%s\n" , msg )
    end
endfunction

