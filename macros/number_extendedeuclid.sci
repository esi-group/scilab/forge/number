// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ d , x , y ] = number_extendedeuclid ( a , b )
    // Solves a linear Diophantine equation (Bezout identity).
    //
    // Calling Sequence
    //   d = number_extendedeuclid ( a , b )
    //   [ d , x ] = number_extendedeuclid ( a , b )
    //   [ d , x , y ] = number_extendedeuclid ( a , b )
    //
    // Parameters
    //   a : a 1x1 matrix of floating point integers, must be positive
    //   b : a 1x1 matrix of floating point integers, must be positive
    //   d : a 1x1 matrix of floating point integers
    //   x : a 1x1 matrix of floating point integers
    //   y : a 1x1 matrix of floating point integers
    //
    // Description
    //   Returns d, x and y such that d==number_gcd(a,b) and
    //
    //<screen>
    //   d = x*a + y*b
    //</screen>
    //
    //   Uses the extended Euclid's algorithm.
    //
    // Examples
    // [ d , x , y ] = number_extendedeuclid ( 99 , 78 ) // [3 -11 14]
    // [ d , x , y ] = number_extendedeuclid ( 120 , 23 ) // [1 -9 47]
    //
    // Bibliography
    //   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
    // http://en.wikipedia.org/wiki/Diophantine_equation
    //
    // Authors
    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_extendedeuclid" , rhs , 2 )
    apifun_checklhs ( "number_extendedeuclid" , lhs , 0:3 )
    //
    // Check type
    apifun_checktype ( "number_extendedeuclid" , a , "a" , 1 , "constant" )
    apifun_checktype ( "number_extendedeuclid" , b , "b" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_extendedeuclid" , a , "a" , 1 )
    apifun_checkscalar ( "number_extendedeuclid" , b , "b" , 2 )
    //
    // Check content
    apifun_checkflint ( "number_extendedeuclid" , a , "a" , 1 )
    apifun_checkrange ( "number_extendedeuclid" , a , "a" , 1 , 0 , 2^53 )
    apifun_checkflint ( "number_extendedeuclid" , b , "b" , 2 )
    apifun_checkrange ( "number_extendedeuclid" , b , "b" , 2 , 0 , 2^53 )
    //
    // Define a sub-function to avoid setup cost
    function [ d , x , y ] = extendedeuclid_re ( a , b )
        if ( b == 0 ) then
            d = a
            x = 1
            y = 0
            return
        end
        [ dp , xp , yp ] = extendedeuclid_re ( b , modulo ( a , b ) )
        d = dp
        x = yp
        y = xp - floor ( a / b ) * yp
    endfunction
    //
    // Proceed
    [ d , x , y ] = extendedeuclid_re ( a , b )
endfunction

