// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function isprime = number_probableprime ( varargin )
  // Check if a number is a probable prime.
  //
  // Calling Sequence
  //   isprime = number_probableprime ( n )
  //   isprime = number_probableprime ( n , s )
  //   isprime = number_probableprime ( n , s , verbose )
  //
  // Parameters
  //   n : a 1x1 matrix of floating point integers, must be positive, the positive integer to test for primality
  //   s : a 1x1 matrix of floating point integers, the number of loops in the test for primality (default 50)
  //   verbose : a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)
  //   isprime : a 1x1 matrix of booleans
  //
  // Description
  //   Returns %f if the number is a composite (surely).
  //   Returns %t if the number is a probable prime (with a small probability of error).
  //
  //   This function uses Miller-Rabin randomized primality test, as
  //   described in "Introduction to algorithms" by Cormen, Leiserson, Rivest, Stein
  //   (see the section 31.8 Primality testing).
  //
  //   If number_probableprime returns %f, therefore we know (i.e. with probability 1)
  //   that n is composite.
  //
  //   If number_probableprime returns %t, therefore we know that n is
  //   a probable prime (with a small probability of error),
  //   but we do not claim that n is prime.
  //   In fact, if n is a probable prime, then the probability
  //   that n is not prime is 2^-s (see Theorem 31.39 in "Introduction to algorithms").
  //   With the default value s=50, the probability that number_probableprime fails is
  //   9e-16, i.e. very small.
  //
  //   This function uses random integers from the grand function (option "uin").
  //
  // Examples
  // // Check for actual primes
  // number_probableprime ( 7 ) // %t
  // number_probableprime ( 5 ) // %t
  // number_probableprime ( 5 , 3 ) // %t
  //
  // // Check for composite numbers
  // number_probableprime ( 10 ) // %f
  // number_probableprime ( 20 ) // %f
  //
  // // Check for pseudo-primes : while pseudo prime does not detect that these numbers
  // // are composite, probable prime does it perfectly.
  // number_probableprime ( 341 ) // %f
  // number_probableprime ( 561 ) // %f
  // number_probableprime ( 645 ) // %f
  // number_probableprime ( 1105 ) // %f
  //
  // // Test verbose option
  // number_probableprime ( 10001 , [] , %t );
  //
  // // Test s option
  // number_probableprime ( 10001 , 10 , %t );
  //
  // // This function may fail if intermediate integers get too large.
  // // An error is generated:
  // number_probableprime ( 4432676798593 )
  //
  // Bibliography
  //   "Introduction to algorithms", Cormen, Leiserson, Rivest, Stein, 2nd edition
  //   http://en.wikipedia.org/wiki/Miller-Rabin_primality_test
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_probableprime" , rhs , 1:3 )
  apifun_checklhs ( "number_probableprime" , lhs , 0:1 )
  //
  n = varargin ( 1 )
  s = apifun_argindefault (  varargin , 2 , 50 )
  verbose = apifun_argindefault (  varargin , 3 , %f )
  //
  // Check type
  apifun_checktype ( "number_probableprime" , n , "n" , 1 , "constant" )
  apifun_checktype ( "number_probableprime" , s , "s" , 2 , "constant" )
  apifun_checktype ( "number_probableprime" , verbose , "verbose" , 3 , "boolean" )
  //
  // Check size
  apifun_checkscalar ( "number_probableprime" , n , "n" , 1 )
  apifun_checkscalar ( "number_probableprime" , s , "s" , 2 )
  apifun_checkscalar ( "number_probableprime" , verbose , "verbose" , 3 )
  //
  // Check content
  apifun_checkflint ( "number_probableprime" , n , "n" , 1 )
  apifun_checkrange ( "number_probableprime" , n , "n" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_probableprime" , s , "s" , 2 )
  apifun_checkrange ( "number_probableprime" , s , "s" , 2 , 1 , 2^53 )
    //
    // Load Internals lib
    path = get_function_path("number_probableprime")
    path = fullpath(fullfile(fileparts(path)))
    numberinternalslib = lib(fullfile(path,"internals"));
  //
  if ( or ( n == [0 1] ) ) then
    isprime = %f
    return
  end
  for j = 1 : s
      if ( verbose ) then
        mprintf ( "Loop %d/%d\n" , j , s )
      end
    // Generates an random integer in [1 , n-1]
    a = grand(1,1,"uin",1,n-1)
    if ( witness ( a , n ) ) then
      if ( verbose ) then
        mprintf ( "Found witness a = %s\n" , string ( a ) )
      end
      isprime = %f
      // Definitely
      return
    end
  end
  isprime = %t
  // Almost surely
endfunction
//
// witness --
//   Returns %t if a is a witness of the compositeness
//   of n, that is, if it is possible to prove that n is composite.
//
function iscomposite = witness ( a , n )
  // Let n-1 = u 2^t where u is odd
  [ t , u ] = factorby2 ( n - 1 )
  xi = number_powermodint ( a , u , n )
  for i = 1 : t
      xnext = number_powermodint ( xi , 2 , n )
    if ( ( xnext == 1 ) & ( xi <> 1 ) & ( xi <> n-1 ) ) then
      iscomposite = %t
      return
    end
    xi = xnext
  end
  if ( xi <> 1 ) then
    iscomposite = %t
  else
    iscomposite = %f
  end
endfunction


//
// factorby2 --
//   Searches for u and t so that n = u 2^t
// Examples
// [ t , u ] = factorby2 ( 12 ), 2^t * u
// [ t , u ] = factorby2 ( 13 ), 2^t * u
// [ t , u ] = factorby2 ( 11 ), 2^t * u
// [ t , u ] = factorby2 ( 10 ), 2^t * u
// [ t , u ] = factorby2 ( 8 ), 2^t * u
//
function [ t , u ] = factorby2 ( n )
  t = 0
  u = n
  while ( %t )
    if ( modulo ( u , 2 ) == 1 ) then
      break
    end
    u = u / 2
    t = t + 1
  end
endfunction

