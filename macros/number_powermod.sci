// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2004 - John Burkardt
//

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = number_powermod ( a, n, m )
    // Modular exponentiation.
    //
    // Calling Sequence
    //   x = number_powermod ( a, n, m )
    //
    // Parameters
    //   a : a 1x1 matrix of floating point integers
    //   n : a 1x1 matrix of floating point integers
    //   m : a 1x1 matrix of floating point integers, must be greater or equal than 1.
    //   x : a 1x1 matrix of floating point integers
    //
    // Description
    //    Returns x = a^n (mod m).
    //
    //    Uses a repeated squaring algorithm.
    //    The algorithm may fail if the number is so large that the
    //    square <literal>a^2</literal> is out of range.
    //
    //    Some programming tricks are used to speed up the computation, and to
    //    allow computations in which A**N is much too large to store in a
    //    real word.
    //
    //    First, for efficiency, the power A**N is computed by determining
    //    the binary expansion of N, then computing A, A**2, A**4, and so on
    //    by repeated squaring, and multiplying only those factors that
    //    contribute to A**N.
    //
    //    Secondly, the intermediate products are immediately "mod'ed", which
    //    keeps them small.
    //
    //    For instance, to compute mod ( A**13, 11 ), we essentially compute 13 = 1 + 4 + 8
    //    then A**13 = A * A**4 * A**8. This leads to
    //    mod ( A**13, 11 ) = mod ( A, 11 ) * mod ( A**4, 11 ) * mod ( A**8, 11 ).
    //
    //    Fermat's little theorem says that if P is prime, and A is not divisible
    //    by P, then ( A**(P-1) - 1 ) is divisible by P.
    //
    // Examples
    // number_powermod( 2 , 3 , 7 ) // 1
    // number_powermod( 4 , 5 , 6 ) // 4
    //
    // // This generates an error, as expected :
    // // intermediate terms are not in the range
    // a = 1028712535855
    // u = 34630287489
    // n = 4432676798593
    // number_powermod( a , u , n ) // 678384139479
	//
	// // If n is negative, then then modular inverse is used.
	// x = number_powermod(3,-3,10) // 3
	// // ... is equivalent to ...
	// t = number_powermod(3,3,10) // 7
	// x = number_inversemod(t,10)
	//
	// // See with negative m
	// x = number_powermod(3,3,-10)
	// // ... is the opposite of ...
	// x = number_powermod(3,3,10)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 2004 - John Burkardt
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_powermod" , rhs , 3 )
    apifun_checklhs ( "number_powermod" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "number_powermod" , a , "a" , 1 , "constant" )
    apifun_checktype ( "number_powermod" , n , "n" , 2 , "constant" )
    apifun_checktype ( "number_powermod" , m , "m" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_powermod" , a , "a" , 1 )
    apifun_checkscalar ( "number_powermod" , n , "n" , 2 )
    apifun_checkscalar ( "number_powermod" , m , "m" , 3 )
    //
    // Check content
    apifun_checkflint ( "number_powermod" , a , "a" , 1 )
    apifun_checkrange ( "number_powermod" , a , "a" , 1 , -2^53 , 2^53 )
    apifun_checkflint ( "number_powermod" , n , "n" , 2 )
    apifun_checkrange ( "number_powermod" , n , "n" , 2 , -2^53 , 2^53 )
    apifun_checkflint ( "number_powermod" , m , "m" , 3 )
    apifun_checkrange ( "number_powermod" , m , "m" , 3 , -2^53 , 2^53 )
    //
    // Load Internals lib
    path = get_function_path("number_powermod")
    path = fullpath(fullfile(fileparts(path)))
    numberinternalslib = lib(fullfile(path,"internals"));
    //
    // Proceed...
	x = number_powermodint ( a, n, m )
endfunction
