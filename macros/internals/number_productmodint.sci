// Copyright (C) 2012 - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_productmodint(a, s, m)
    // Computes a*s modulo m.
    //
    // Calling Sequence
    //   p = number_productmodint ( a , s , m )
    //
    // Parameters
    // a : a 1x1 matrix of floating point integers.
    // s : a 1x1 matrix of floating point integers
    // m : a 1x1 matrix of floating point integers, the modulo
    // p : a 1x1 matrix of floating point integers, the result of a*s modulo m.
    //
    // Description
    // Returns the result of a*s (modulo m).
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    if m == 0 then
        p = 0;
        return;
    end
    a = pmodulo(a, m);
    s = pmodulo(s, m);
    as = a * s;
    if abs(as) < 2^53 then
        p = pmodulo(as, m);
    else
        aass = min(a^2, s^2);
        if aass <= m then
            if a > s then
                p = number_prodmodshrage(s, a, m);
            else
                p = number_prodmodshrage(a, s, m);
            end
        else
            p = number_prodmodrecred(a, s, m);
        end
    end
endfunction
