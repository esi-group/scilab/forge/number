// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function result = number_gcdeuclidint ( m , n )
  //   Returns the greatest common divisor of m and n
  //
  // Calling Sequence
  //   result = number_gcd ( m , n )
  //   result = number_gcd ( m , n , method )
  //
  // Parameters
  // m : a 1x1 matrix of floating point integers
  // n : a 1x1 matrix of floating point integers
  // result : a 1x1 matrix of floating point integers, the least common multiple
  //
  // Description
  //   Use a naive recursive Euclidian method.
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

  if ( m==0 ) then
       result = n
       return
    end
    if ( n==0 ) then
       result = m
       return
    end
    if ( m>=n ) then
        r = modulo ( m , n )
        result = number_gcdeuclidint ( r , n )
    else
        r = modulo ( n , m )
        result = number_gcdeuclidint ( m , r )
    end
endfunction
