<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from number_gcd.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="number_gcd" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>number_gcd</refname><refpurpose>Greatest common divisor.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   result = number_gcd ( m , n )
   result = number_gcd ( m , n , method )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>m :</term>
      <listitem><para> a 1x1 matrix of floating point integers</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1x1 matrix of floating point integers</para></listitem></varlistentry>
   <varlistentry><term>method :</term>
      <listitem><para> a 1x1 matrix of strings, the algorithm to use (default method=euclidian)</para></listitem></varlistentry>
   <varlistentry><term>result :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the least common multiple</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the greatest common divisor of m and n.
   </para>
   <para>
The following values of method are availble.
<variablelist>
<varlistentry>
<term> method="euclidian" </term>
<listitem>
<para>
Uses a naive Euclid's recursive algorithm.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term> method="brute" </term>
<listitem>
<para>
Uses a brute-force method, based on systematic search
for divisors.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term> method="binary" </term>
<listitem>
<para>
Uses a naive binary recursive method.
This is similar to Algorithm B in section 4.5.2 of TAO.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term> method="euclidianiterative" </term>
<listitem>
<para>
Uses Euclid's algorithm with a loop (instead of recursive).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term> method="binaryiterative" </term>
<listitem>
<para>
Uses binary algorithm with a loop (instead of recursive).
</para>
</listitem>
</varlistentry>
</variablelist>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
number_gcd ( 84 , 18 ) // 6
number_gcd ( 18 , 84 ) // 6

// With negative integers
computed = number_gcd ( 18 , -84 ) // 6

// Test all algos
algos = [
"euclidian"
"brute"
"binary"
"euclidianiterative"
"binaryiterative"
];
mprintf("  %s\n", strcat(algos," "));
for m = 1 : 20
for n = 1 : 20
msg="";
msg=msg+msprintf("gcd(%d,%d) = ",m, n);
for method = algos'
d = number_gcd (m,n,method);
msg=msg+msprintf("%s ",string(d));
end
msg=msg+msprintf("\n");
mprintf("%s\n",msg);
end
end

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"The Art of Computer Programming", Volume 2, Seminumerical Algorithms, Donald Knuth, Addison-Wesley</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) INRIA - Farid BELAHCENE</member>
   </simplelist>
</refsection>
</refentry>
