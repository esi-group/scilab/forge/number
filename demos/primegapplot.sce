// Copyright (C) 2010 - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoPrimegapplot()

    //
    // References
    // http://mathworld.wolfram.com/PrimeDifferenceFunction.html
    n=500;
    mprintf("Plots the gap of primes")
    mprintf("For all primes lower than n=%d\n",n)
    allprimes = number_primes(n);
    np = size(allprimes,"*");
    gaps = diff(allprimes);
    ng=np-1;
    scf();
    plot(1:ng,gaps)
    xtitle("Gap between two primes","Nth prime","Gap to the next prime")

    //========= E N D === O F === D E M O =========//
    //
    // Load this script into the editor
    //
    filename = "primegapplot.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction
demoPrimegapplot();
clear demoPrimegapplot
