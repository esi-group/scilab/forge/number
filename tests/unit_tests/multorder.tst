// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// The order of 3 modulo 10 is 4
k = number_multorder( 3,  10);
assert_checkequal(k,4);

// The order of 5 modulo 12 is 2
k = number_multorder( 5,  12);
assert_checkequal(k,2);

// The order of 7 modulo 24 is 2.
k = number_multorder( 7,  24);
assert_checkequal(k,2);

// The order of 3 modulo 7 is 6.
k = number_multorder(3, 7);
assert_checkequal(k,6);

// The order of 4 modulo 25 is 10.
k = number_multorder(4, 25);
assert_checkequal(k,10);
//
k = number_multorder(37,1000);
assert_checkequal(k,100);
//
k = number_multorder(54,100001);
assert_checkequal(k,9090);
//

function checkmultiorder(a,m)
    k = number_multorder( a,  m)
    u = number_powermod(a,k,m)
    assert_checkequal(u,1)
    for i = 1 : k-1
        u = number_powermod(a,i,m)
        assert_checktrue(u<>1)
    end
endfunction


for m = 1 : 3 : 20
    for a = 1 : 3: 20
        d = number_gcd(a,m);
        if (d==1) then
            mprintf("Checking number_multorder(%d,%d)...",a,m)
            checkmultiorder(a,m)
            mprintf("OK\n")
        end
    end
end

