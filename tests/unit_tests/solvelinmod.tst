// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt




assert_checkequal ( number_solvelinmod ( 14 , 30 , 100 ) , [95 45] );
assert_checkequal ( number_solvelinmod ( 3 , 4 , 5 ) , 3 );
assert_checkequal ( number_solvelinmod ( 3 , 5 , 6 ) , [] );
assert_checkequal ( number_solvelinmod ( 3 , 6 , 9 ) , [2 5 8] );

