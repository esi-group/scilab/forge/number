// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

t = number_productmod ( 5 , 3 , 31 );
tt = pmodulo(5*3,31);
assert_checkequal(t,tt);
//
t = number_productmod ( 3 , 5 , 31 );
tt = pmodulo(5*3,31);
assert_checkequal(t,tt);
//
t = number_productmod ( 6 , 4 , 41 );
tt = pmodulo(6*4,41);
assert_checkequal(t,tt);
//
t = number_productmod ( 3 , 1 , 11 )
tt = pmodulo(3*1,11);
assert_checkequal(t,tt);
//
function checkproductmod(a,s,m)
    mprintf("Checking number_productmod(%d,%d,%d)...",a,s,m)
    t = number_productmod ( a , s , m )
    z = pmodulo(a*s,m)
    assert_checkequal(t,z)
    mprintf("OK\n");
endfunction

for m = 2 : 5 : 40
    for a = 1 : 7 : m
        for s = 1 : 11 : 10
            checkproductmod(a,s,m)
            checkproductmod(s,a,m)
        end
    end
end

//
// Check with large numbers
//
// Case 3
t = number_productmod(106034106,106034106,137568061);
assert_checkequal(t,86644614);
//
// Case 2
t = number_productmod(106034107,106034106,137568061);
assert_checkequal(t,55110659);
//
// Case 1
t = number_productmod(2^16,2^15,2^50);
assert_checkequal(t,2147483648);
//
// Case 4
t = number_productmod(2^16,(2^47-1),2^50);
assert_checkequal(t,1125899906777088);
// Lemma:
// Suppose that x = a*s (mod m)
// and y = (a+k)*s (mod m)
// Therefore y = x+t (mod m), where t = k*s (mod m)
// Proof:
// y = (a+k)*s (mod m)
//   = a*s+k*s (mod m)
//   = (a*s (mod m) + k*s (mod m)) (mod m)
//   = (x + t) (mod m)
// Similarily, if y = (a-k)*s (mod m)
// therefore, y = x-t (mod m), where t = k*s (mod m).
// Use this lemma to test number_productmod.
function checkShiftedA(a, s, m,imax)
    // Parameters
    // a : a 1x1 matrix of floating point integers.
    // s : a 1x1 matrix of floating point integers
    // m : a 1x1 matrix of floating point integers, the pmodulo
    x = number_productmod(a, s, m);
    for k = floor(linspace(1, m-1, imax))
        t = number_productmod(k, s, m);
        //
        // 1. See a+k
        mprintf("number_productmod(%s,%s,%s)\n",string(a+k),string(s),string(m));
        p = number_productmod(a+k, s, m);
        y = pmodulo(x+t, m);
        assert_checkequal(p, y);
        //
        // 2. Switch a+k and s
        mprintf("number_productmod(%s,%s,%s)\n",string(s),string(a+k),string(m));
        p = number_productmod(s, a+k, m);
        y = pmodulo(x+t, m);
        assert_checkequal(p, y);
        //
        // 3. See a-k
        mprintf("number_productmod(%s,%s,%s)\n",string(a-k),string(s),string(m));
        p = number_productmod(a-k, s, m);
        y = pmodulo(x-t, m);
        assert_checkequal(p, y);
        //
        // 4. Switch a-k and s
        mprintf("number_productmod(%s,%s,%s)\n",string(s),string(a-k),string(m));
        p = number_productmod(s, a-k, m);
        y = pmodulo(x-t, m);
        assert_checkequal(p, y);
    end
endfunction
//
// x = pmodulo(106034106*106034106, 137568061) = 86644614
// This is a case where as > 2^53,
// and a^2>m and s^2>m.
a = 106034106;
s = a;
m = 137568061;
p = number_productmod(a, s, m);
assert_checkequal(p,86644614);
checkShiftedA(a, s, m, 11);
//
// pmodulo(2199023255551*2199023255551,8796093022207) = 4947802324992
// This is a case where as > 2^53,
// and a^2>m and s^2>m.
a = 2199023255551;
s = a;
m = 8796093022207;
p = number_productmod(a, s, m);
assert_checkequal(p, 4947802324992);
checkShiftedA(a, s, m, 11);
//
// This is a case where a^2<m and s^2<m
a = 2965820;
s = a;
m = 8796093022207;
p = number_productmod(a, s, m);
assert_checkequal(p,8796088272400);
checkShiftedA(a, s, m, 11);
//
// Check negative arguments
p = number_productmod ( -6 , 4 , 41 );
assert_checkequal(p, 17); // -17 if modulo was used internally
p = number_productmod ( 6 , -4 , 41 );
assert_checkequal(p, 17); // -17 if modulo was used internally
p = number_productmod ( 6 , 4 , -41 );
assert_checkequal(p, 24); // 17 if modulo was used internally
//
p = number_productmod(3, 3, 0);
assert_checkequal(p, 0);
