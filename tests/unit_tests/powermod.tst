// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



x = number_powermod( 2 , 3 , 7 );
assert_checkequal ( x , 1 );
x = number_powermod( 4 , 5 , 6 );
assert_checkequal ( x , 4 );

// Test with large values.
a = 1028712535855;
u = 34630287489;
n = 4432676798593;
x = number_powermod( a , u , n );
assert_checkequal ( x , 678384139479 );

// Test with negative entries
x = number_powermod(-1,3,1000);
assert_checkequal ( x , 999 );
//
x = number_powermod(3,-3,10);
assert_checkequal ( x , 3 );
//
x = number_powermod(3,3,-10);
assert_checkequal ( x , -7 );
//
x = number_powermod(3,3,0);
assert_checkequal ( x , 0 );
