// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


[gap,p] = number_maximalprimegap ( 1500 );
expected_gap = [1 2 4 6 8 14 18 20 22 34]';
expected_p = [2 3 7 23 89 113 523 887 1129 1327]';
assert_checkequal ( gap , expected_gap );
assert_checkequal ( p , expected_p );

