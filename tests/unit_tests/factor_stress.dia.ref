// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
//
// Stress-tests
// Check that a reasonable behaviour occurs when the
// factoring fails.
// The reasonable behaviour is that any factor that may have been discovered
// should be stored in the final result.
//
// Test complete failure
// Try with 2 large primes
// 7349 and 7901 are two primes
n = 7349 * 7901;
[ computed , status ] = number_factor ( n , "pollard-rho" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , n );
[ computed , status ] = number_factor ( n , "trialdivision" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , n );
[ computed , status ] = number_factor ( n , "memorylesstd" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , n );
// A difficult test for Fermat's
// 33554431  = 1801 * 18631 = 1801 * ( 31  *  601 )
n = 1801 * 31  *  601 ;
[ computed , status ] = number_factor ( n , "fermat" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , n );
// Test partial failure : check that factors which have been found
// are correctly returned
// Try with 2 large primes
// 7349 and 7901 are two primes
n = 2 * 3 * 7349 * 7901;
// Too large for probableprime
//[ computed , status ] = number_factor ( n , "pollard-rho" , 10 );
//assert_checkequal ( status , %f );
//assert_checkequal ( computed , [ 2 3 58064449 ] );
[ computed , status ] = number_factor ( n , "trialdivision" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , [ 2 3 58064449 ] );
[ computed , status ] = number_factor ( n , "memorylesstd" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , [ 2 3 58064449 ] );
[ computed , status ] = number_factor ( n , "memorylesstd" , 10 );
assert_checkequal ( status , %f );
assert_checkequal ( computed , [ 2 3 58064449 ] );
// A partial factoring case for Fast trial division
n = 2^12;
[ computed , status ] = number_factor ( n , "fasttrialdivision" , 10 );
assert_checkequal ( status , %f );
expected = [2.    2.    2.    2.    2.    2.    2.    2.    2.    2.    4.];
assert_checkequal ( computed , expected );
