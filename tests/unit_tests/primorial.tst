// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



computed = number_primorial ( 2 );
assert_checkequal ( computed , 2 );

computed = number_primorial ( 3 );
assert_checkequal ( computed , 2*3 );

computed = number_primorial ( 4 );
assert_checkequal ( computed , 2*3 );

computed = number_primorial ( 7 );
assert_checkequal ( computed , 2*3*5*7 );

